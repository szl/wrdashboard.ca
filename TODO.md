# TODO for feeds which could not be pulled

Sites which do not have feeds which we can read, figure how to scrap them in the future.

## Articles

* [CBC Kitchener-Waterloo](http://www.cbc.ca/news/canada/kitchener-waterloo), [feed](https://www.cbc.ca/cmlink/rss-canada-kitchenerwaterloo)
* [The Record](http://www.therecord.com/waterlooregion/), has a large number of RSS feeds but not singular one
* [Kitchener Citizen](http://www.kitchenercitizen.com)
* [Waterloo Chronicle](http://www.waterloochronicle.ca/waterloo-on/)
* [Kitchener Post](http://www.kitchenerpost.ca/kitchener-on/)
* [Snapd - Kitchener-Waterloo](https://kitchenerwaterloo.snapd.com/)
* [Inside the Perimeter](https://insidetheperimeter.ca/)
* [Good Company Magazine](http://goodcomagazine.com/)
* [Aashni Shah](http://blog.aashni.me/)
* [Betakit Waterloo](https://betakit.com/tag/waterloo/)
* [Joseph Fung](http://www.josephfung.ca/)
* [Culture Fancier](http://www.culturefancier.com/)
* [Joe Kidwell](http://joekidwell.net/)
* [Charlie Drage](http://charliedrage.com/blog/)

## Audio

* [Centre for International Governance Innovation](https://www.cigionline.org/multimedia#block-views-block-videos-block-5)
* [Nimble Hippo Radio](https://soundcloud.com/communitech-hub)

## Video

* [Centre for International Governance Innovation](https://www.cigionline.org/multimedia#block-views-block-videos-block-4)
